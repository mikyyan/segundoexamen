<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table='student';

    protected $primarykey='id_student';
    public $TIMESTAMPS=true;
    const CREATE_AT='create_ad';
    const UPDATED_AT='update_ad';

    protected $fillabel=[
        'name_student',
        'last_name_student',
        'age_student',
        'fono_student',
        'email_student',
        'address_student',
        'create_ad',
        'update_ad'
    ];
}
