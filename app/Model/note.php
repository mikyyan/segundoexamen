<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class note extends Model
{
    protected $table='note';

    protected $primarykey='cod_note';
    public $TIMESTAMPS=true;
    const CREATE_AT='create_ad';
    const UPDATED_AT='update_ad';

    protected $fillabel=[
        'name_matter',
        'first_note',
        'second_note',
        'final_note',
        'create_ad',
        'update_ad'
    ];
}
