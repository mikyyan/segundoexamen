<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class enrollment extends Model
{
    protected $table='enrollment';

    protected $primarykey='cod_enrollment';
    public $TIMESTAMPS=true;
    const CREATE_AT='create_ad';
    const UPDATED_AT='update_ad';

    protected $fillabel=[
        'name_matter',
        'capacity',
        'create_ad',
        'update_ad'
    ];
}
