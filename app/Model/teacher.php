<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class teacher extends Model
{
    protected $table='teacher';

    protected $primarykey='id_teacher';
    public $TIMESTAMPS=true;
    const CREATE_AT='create_ad';
    const UPDATED_AT='update_ad';

    protected $fillabel=[
        'name_teacher',
        'last_name_teacher',
        'age_teacher',
        'fono_teache',
        'email_teacher',
        'address_teacher',
        'create_ad',
        'update_ad'
    ];
}
